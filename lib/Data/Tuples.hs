{-# LANGUAGE TypeFamilies, TypeFamilyDependencies, MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances, FlexibleContexts #-}

module Data.Tuples
  (
    -- * Tuple Access
    Head(..), Tail(..), Init(..), Last(..)
  , Final(..)
    -- * Tuple Manipulation
    -- ** Shrinking
  , Shrinkable(..)
  , crush
    -- ** Growing
  , Grow(..)
  , Growable(..)
  , smear
    -- * Utilities
  -- , swap
  ) where

import Prelude hiding (last)
-- import Data.Tuple

---

{-
GOALS:

- `curry` and `uncurry` generalized to any size of tuple, ala `printf`
- Pushing, popping
- Generalized `(***)`: applying a tuple of functions to a tuple of items
- Generalized `(&&&)`: applying a tuple of functions to /one/ item, "fanning out" the results.
- Folding tuples
- splicing tuples
- strict tuples
- generalized access to each index
- Foldable instance for tuples

-}

type family Head t where
  Head (a,b,c,d) = a
  Head (a,b,c)   = a
  Head (a,b)     = a

type family Tail t where
  Tail (a,b,c,d) = (b,c,d)
  Tail (a,b,c)   = (b,c)
  Tail (a,b)     = b

type family Init t where
  Init (a,b,c,d) = (a,b,c)
  Init (a,b,c)   = (a,b)
  Init (a,b)     = a

type family Last t where
  Last (a,b,c,d) = d
  Last (a,b,c)   = c
  Last (a,b)     = b

class Shrinkable t where
  uncons :: t -> (Head t, Tail t)
  unsnoc :: t -> (Init t, Last t)

  init :: t -> Init t
  init = fst . unsnoc

  tail :: t -> Tail t
  tail = snd . uncons

instance Shrinkable (a,b,c,d) where
  uncons (a,b,c,d) = (a, (b,c,d))
  unsnoc (a,b,c,d) = ((a,b,c), d)

instance Shrinkable (a,b,c) where
  uncons (a,b,c) = (a, (b,c))
  unsnoc (a,b,c) = ((a,b), c)

instance Shrinkable (a,b) where
  uncons = id
  unsnoc = id

type family Grow t r where
  Grow (a,b,c) r = (a,b,c,r)
  Grow (a,b) r   = (a,b,r)

class Growable t where
  grow  :: r -> t -> Grow t r

instance Growable (a,b) where
  grow r (a,b) = (a,b,r)

instance Growable (a,b,c) where
  grow r (a,b,c) = (a,b,c,r)

-- | Transform the right-most element of a tuple.
class Final t where
  last  :: t a -> Last (t a)
  final :: (Last (t a) -> b) -> t a -> t b

instance Final ((,) a) where
  last (_, b) = b
  final = fmap

instance Final ((,,) a b) where
  last (_, _, c)  = c
  final f (a,b,c) = (a, b, f c)

instance Final ((,,,) a b c) where
  last (_, _, _, d) = d
  final f (a,b,c,d) = (a, b, c, f d)

-- | Merge the two right-most elements of a tuple.
--
-- @
-- >>> crush (+) (1,2,3,4)
-- (1,2,7)
-- @
crush :: (Shrinkable t1, Final t2, Init t1 ~ t2 a) => (Last (t2 a) -> Last t1 -> b) -> t1 -> t2 b
crush f t = case unsnoc t of (i,l) -> final (`f` l) i

-- | Apply a transformation to the right-most element of a tuple, appending the
-- result to the end.
--
-- @
-- >>> smear (* 5) (1,2,3)
-- (1,2,3,15)
-- @
smear :: (Growable (t1 a1), Final t2, Final t1, Grow (t1 a1) (Last (t1 a1)) ~ t2 a2) =>
  (Last (t2 a2) -> b) -> t1 a1 -> t2 b
smear f t = final f $ grow (last t) t
